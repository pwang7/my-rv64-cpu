# my-rv64-cpu
本项目是基于“一生一芯”项目提供的CPU开发框架(oscpu-framework)实现的64位RISC-V顺序单发射五级流水线CPU核

[一生一芯官网]: https://oscpu.github.io/ysyx/



### 开发环境及框架

操作系统：Linux Ubuntu v20.04

开发软件：verilator、gtkwave

开发仿真框架：oscpu-framework

具体的开发环境配置及oscpu-framework框架的下载和使用详见github地址：https://github.com/OSCPU/oscpu-framework.git

note：要运行本仓库所含源文件，需按oscpu-framework文档要求添加到该开发框架下才可仿真运行；



### CPU架构概略图

![framework](framwork_picture/framework.jpg)



### 项目文件和对应模块简介

1) defines.v：该文件中定义了CPU核代中常用的相关宏定义；

2. pc.v：该文件实现了CPU核中的程序计数器PC(Program Counter)模块;

3. if_id.v：该文件实现了流水线中取指模块和译码模块间的寄存器if_id; 

4. id.v：该文件实现了CPU核中负责译码功能的id模块；

5. mini_id_bpu.v：该文件实现了CPU核中负责静态分支预测的模块mini_id_bpu;

6. id_ex.v：该文件实现了流水线中译码模块和执行模块间的寄存器id_ex;

7. ex.v：该文件实现了CPU核中的执行模块ex, 负责算术运算等功能；
8. csr.v：该文件实现了RISC-V架构定义的控制与状态寄存器CSR;

9. ex_mem.v：该文件实现了流水线中执行模块和访存模块之间的寄存器ex_mem;

10. mem.v：该文件实现了CPU核中负责访存的模块mem；

11. mem_wb.v：该文件实现了流水线中访存模块和写回模块间的寄存器mem_wb；

12. regfile.v：该文件实现了CPU核中的寄存器堆模块regfile；

13. biu.v：该文件实现了CPU核的总线接口模块biu，主要负责将CPU核的访存信号转换为支持AXI-4协议的访存信号，同时还负责仲裁取指模块和访存模块两者同时发出的访存请求；

14. pipeline_ctrl.v：该文件实现了CPU核中的流水线控制模块，该模块的输入为各级流水线的暂停请求，输出为对各级流水线的控制信号；

15. rvcpu.v：该文件实现了rvcpu模块，即所实现的RISC-V处理器核。该文件中实例化了模块：pc、if_id、id、mini_id_bpu、id_ex、ex、csr、ex_mem、mem、mem_wb、regfile、biu、pipeline_ctrl；

16. clint.v：该文件实现了CPU核中的clint (core local interruptor) 模块，该模块中实现了RISC-V架构中的mtime寄存器和mtimecmp寄存器，负责产生定时器中断；

17. axi_splt.v：该文件实现了总线分发模块axi_splt，该模块将来自biu模块的一组AXI-4信号，根据访问地址，分发给通向CLINT模块的AXI-4总线和通向核外的AXI总线；

18. simuart.v：该文件实现了仿真用的uart模块

19. SimTop.v：该文件中实现了仿真顶层模块SimTop，该模块的接口按照difftest测试框架的要求设定，实例化的模块包括rvcpu、axi_splt、clint、sim_uart；

    

### 详细设计思路

待更新......



